#!/usr/bin/env python3

from setuptools import setup, find_packages

from reidentification import __version__

setup(
    name='Reidentification',
    version=__version__,
    packages=find_packages(),

    entry_points={
        'console_scripts': (
            'Reidentification = reidentification.__main__:cli',
        )
    },

    install_requires=[
        'click',
        'numpy',
        'scikit-image',
        'scikit-learn',
        'torchvision',
    ],
    extras_require={
        'visualization': [
            'pydot'
            'graphviz'
            'matplotlib'
        ],
    }
)
