import numpy as np
import torchvision
from PIL import Image
from typing import Tuple, Optional


def load_image(path: str, forced_size: Optional[Tuple[int, int]]=None) -> np.ndarray:
    transformations = []
    image = Image.open(path)
    if forced_size is not None:
        transformations.append(torchvision.transforms.Resize(forced_size))

    transformations.append(torchvision.transforms.RandomHorizontalFlip())
    transformations.append(torchvision.transforms.ToTensor())

    return torchvision.transforms.Compose(transformations)(image)
