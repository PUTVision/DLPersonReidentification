import torch


def rank_accuracy(cdist, pids, rank):
    batch_size = cdist.size()[0]
    index = torch.topk(cdist, rank + 1, largest=False, dim=1)[1]
    index = index[:, 1:]

    topk = torch.zeros(cdist.size()[0]).byte()
    topk = topk.cuda()
    topks = []
    for c in index.split(1, dim=1):
        c = c.squeeze()  # c is batch_size x 1
        topk = topk | (pids.data == pids[c].data)
        acc = torch.sum(topk) / batch_size
        topks.append(acc)

    return topks