import random

from reidentification.evaluation.mars import Predictions


def get_rank_accuracy(predictions: Predictions, rank: int) -> float:
    valid_predictions = 0
    all_predictions = 0
    for source_person_id, source_cameras in predictions.items():
        for target_cameras in source_cameras.values():
            for person_predictions in target_cameras.values():
                random.shuffle(person_predictions)  # Fix for a situation where some distances are the same
                sorted_person_predictions = sorted(person_predictions, key=lambda tup: tup[1], reverse=True)
                target_persons_ids = [person_id for person_id, _ in sorted_person_predictions]
                if int(source_person_id) in target_persons_ids[:rank]:
                    valid_predictions += 1

                all_predictions += 1

    return valid_predictions / all_predictions
