import numpy as np
from abc import ABCMeta, abstractmethod


class Predictor(metaclass=ABCMeta):
    @abstractmethod
    def predict(self, *args) -> np.ndarray:
        pass
