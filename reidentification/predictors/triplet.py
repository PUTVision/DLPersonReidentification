from typing import Union

import numpy as np
import torch

from reidentification.predictors.predictor import Predictor


class TripletPredictor(Predictor):
    def __init__(self, model: Union[torch.nn.Module], device: torch.device):
        self._model = model
        self._device = device

    def predict(self, image: np.ndarray) -> np.ndarray:
        with torch.no_grad():
            tensor = torch.from_numpy(image).to(self._device)
            return self._model(tensor).data.cpu().numpy()
