from typing import Optional

import click

from reidentification.training.pytorch import validate


@click.group()
def utils():
    pass


@utils.command()
@click.argument('predictions_path', type=click.Path(exists=True, dir_okay=False))
@click.option('--tabular-output', is_flag=True)
def calculate_rank_accuracy(predictions_path: str, tabular_output: bool):
    import json

    from reidentification.metrics.rank_accuracy import get_rank_accuracy

    with open(predictions_path) as predictions_file:
        predictions = json.load(predictions_file)

    if tabular_output:
        line = ''
        for rank in (1, 5, 10, 20):
            if rank != 1:
                line += ';'

            line += str(get_rank_accuracy(predictions, rank))

        print(line)
    else:
        for rank in (1, 5, 10, 20):
            print(f'Rank {rank} accuracy: {get_rank_accuracy(predictions, rank)}')


@utils.command()
@click.argument('predictions_path', type=click.Path(exists=True, dir_okay=False))
@click.argument('rank', type=int)
def generate_sample_images(predictions_path: str, rank: int):
    import json

    with open(predictions_path) as predictions_file:
        predictions = json.load(predictions_file)

    valid_prediction_images = None
    invalid_prediction_images = None

    for source_person_name, source_cameras in predictions.items():
        for source_camera_name, target_cameras in source_cameras.items():
            for target_camera_name, person_predictions in target_cameras.items():
                sorted_person_predictions = sorted(person_predictions, key=lambda tup: tup[1], reverse=True)
                target_persons_names = [person_name for person_name, _ in sorted_person_predictions]
                if source_person_name in target_persons_names[:rank]:
                    if valid_prediction_images is None:
                        valid_prediction_images = (source_person_name, source_camera_name,
                                                   target_persons_names[:rank], target_camera_name)
                else:
                    if invalid_prediction_images is None:
                        invalid_prediction_images = (source_person_name, source_camera_name,
                                                     target_persons_names[:rank], target_camera_name)

                if valid_prediction_images is not None and invalid_prediction_images is not None:
                    print(valid_prediction_images, invalid_prediction_images)
                    return


@utils.command()
@click.argument('model_name', type=str)
@click.argument('dataset_path', type=click.Path(exists=True, file_okay=False))
@click.option('--persons-per-batch', type=int, default=18)
@click.option('--images-per-person', type=int, default=4)
@click.option('--pretrained', is_flag=True)
@click.option('--custom-weights', type=click.Path(exists=True, dir_okay=False), default=None)
@click.option('--cpu', is_flag=True)
def find_triplet_optimizer_parameters(model_name: str, dataset_path: str, persons_per_batch: int,
                                      images_per_person: int, pretrained: bool, custom_weights: Optional[str],
                                      cpu: bool):
    import os
    import random
    import importlib
    import math

    import torch.utils.data
    import numpy as np

    from sklearn.model_selection import train_test_split

    from reidentification.generators.triplet_batch_hard import TripletSequence
    from reidentification.losses.triplet_batch_hard import BatchHardTripletLoss
    from reidentification.generators.triplet_batch_hard import TripletSampler

    model_module = importlib.import_module(f'reidentification.models.{model_name}')

    np.random.seed(2018)
    random.seed(2018)

    image_size = (224, 224) if pretrained else (256, 128)
    print(f'Image size: {image_size}')
    input_shape = (3, *image_size)

    persons = os.listdir(dataset_path)
    training_persons, validation_persons = train_test_split(persons, test_size=0.25, random_state=2018)

    device = torch.device('cuda') if not cpu else torch.device('cpu')

    training_sequence = TripletSequence(data_directory_path=dataset_path, persons=training_persons,
                                        image_size=image_size, device=device)
    validation_sequence = TripletSequence(data_directory_path=dataset_path, persons=validation_persons,
                                          image_size=image_size, device=device)

    training_loader = torch.utils.data.DataLoader(
        training_sequence, batch_sampler=TripletSampler(training_sequence, persons_per_batch, images_per_person),
        num_workers=4, pin_memory=True
    )
    validation_loader = torch.utils.data.DataLoader(
        validation_sequence, batch_sampler=TripletSampler(validation_sequence, persons_per_batch, images_per_person),
        num_workers=4, pin_memory=True
    )

    batch_hard_triplet_loss = BatchHardTripletLoss(device)

    for lr in (1e-1, 3e-2, 1e-2, 3e-3, 1e-3, 3e-4, 1e-4, 3e-5, 1e-5, 3e-6, 1e-6):
        for eps in (1e0, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8):
            model = model_module.get_model(input_shape, pretrained=pretrained,
                                           custom_weights=custom_weights)
            print(f'Trying lr = {lr}, eps = {eps}')
            optimizer = torch.optim.Adam(model.parameters(), lr=lr, eps=eps)

            smallest_loss = float('inf')
            smallest_loss_epoch = 0
            for epoch in range(80):
                model = model.train()
                loss_sum = 0.0

                for inputs, labels in training_loader:
                    labels = labels.to(device)

                    optimizer.zero_grad()
                    outputs = model(inputs)
                    loss = batch_hard_triplet_loss(outputs, labels)
                    loss.backward()
                    optimizer.step()

                    loss_sum += loss.item()

                loss = loss_sum / len(training_loader)
                val_loss = validate(model, batch_hard_triplet_loss, validation_loader, device)
                print(f'Epoch {epoch + 1}: loss: {loss:.4f}; val_loss: {val_loss:.4f}')

                if loss >= 10 or math.isnan(loss):
                    break

                if val_loss < smallest_loss:
                    smallest_loss = val_loss
                    smallest_loss_epoch = epoch

                if epoch - smallest_loss_epoch >= 15:
                    break

            if val_loss < 0.69:
                print(f'Result: Success with lr = {lr}, eps = {eps}: loss {loss}; val_loss {val_loss}')
            else:
                print(f'Result: Failure with lr = {lr}, eps = {eps}: loss {loss}; val_loss {val_loss}')


@utils.command()
@click.argument('vectors_path', type=click.Path(exists=True, dir_okay=False))
def print_predicted_vectors(vectors_path: str):
    import pickle

    with open(vectors_path, 'rb') as output_file:
        predicted_vectors = pickle.load(output_file)

    for person_name, person_cameras in predicted_vectors.items():
        for camera_id, vector in person_cameras.items():
            print(f'Person {person_name}, camera {camera_id}: {vector}')


@utils.command()
@click.argument('vectors_path', type=click.Path(exists=True, dir_okay=False))
@click.argument('output_path', type=click.Path(dir_okay=False))
@click.option('--kissme-matrix-path', type=click.Path(exists=True, dir_okay=False))
def vectors_to_predictions(vectors_path: str, output_path: str, kissme_matrix_path: Optional[str]):
    import pickle
    import json

    from reidentification.evaluation.mars import generate_triplet_predictions

    with open(vectors_path, 'rb') as input_file:
        predicted_vectors = pickle.load(input_file)

    if kissme_matrix_path is not None:
        with open(kissme_matrix_path, 'rb') as input_file:
            kissme_matrix = pickle.load(input_file)
    else:
        kissme_matrix = None

    predictions = generate_triplet_predictions(vectors=predicted_vectors, kissme_matrix=kissme_matrix)
    with open(output_path, 'w') as output_file:
        json.dump(predictions, output_file)
