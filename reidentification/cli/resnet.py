from typing import Optional

import click


@click.group()
def resnet():
    pass


@resnet.command()
@click.argument('dataset_type', type=click.Choice(['mars', 'market']))
@click.argument('dataset_path', type=click.Path(exists=True, file_okay=False))
@click.argument('model_path', type=click.Path(dir_okay=False))
@click.option('--lr', type=float, default=2e-04)
@click.option('--eps', type=float, default=1e-08)
@click.option('--persons-per-batch', type=int, default=32)
@click.option('--images-per-person', type=int, default=4)
@click.option('--steps-between-validations', type=int, default=500)
@click.option('--continue-training', is_flag=True)
@click.option('--pretrained', is_flag=True)
@click.option('--cpu', is_flag=True)
def train_triplet(dataset_type: str, dataset_path: str, model_path: str, lr: float, eps: float, persons_per_batch: int,
                  steps_between_validations: int, images_per_person: int, continue_training: bool, pretrained: bool,
                  cpu: bool):
    import random

    import numpy as np
    import torch.utils.data
    import torch.utils.data.sampler

    from reidentification.generators.triplet_batch_hard import TripletSequence, TripletSampler
    from reidentification.training import pytorch
    from reidentification.losses.triplet_batch_hard_2 import TripletLoss
    from reidentification.models.resnet50 import ResNet50
    from reidentification.generators.loaders.mars import MarsLoader
    from reidentification.generators.loaders.market1501 import MarketLoader

    image_size = (128, 256)

    np.random.seed(42)
    random.seed(42)

    device = torch.device('cuda') if not cpu else torch.device('cpu')

    if dataset_type == 'mars':
        loader = MarsLoader(dataset_path, images_per_person)
    elif dataset_type == 'market':
        loader = MarketLoader(dataset_path, images_per_person)
    else:
        raise NotImplementedError

    training_images, training_persons_ranges = loader.training_data
    validation_query_data, validation_gallery_data = loader.validation_query_data, loader.validation_gallery_data
    training_sequence = TripletSequence(images=training_images, persons_ranges=training_persons_ranges,
                                        image_size=image_size, resize_images=True, device=device)

    training_loader = torch.utils.data.DataLoader(
        training_sequence, batch_sampler=TripletSampler(training_sequence, persons_per_batch, images_per_person),
        num_workers=4, pin_memory=True
    )

    batch_hard_triplet_loss = TripletLoss()

    model = ResNet50(pretrained=pretrained)
    if not cpu:
        model = torch.nn.DataParallel(model).to(device)
    if continue_training:
        model.load_state_dict(torch.load(model_path))

    optimizer = torch.optim.Adam(model.parameters(), lr=lr, eps=eps)
    pytorch.train(model=model, model_path=model_path, loss_function=batch_hard_triplet_loss,
                  optimizer=optimizer, initial_learning_rate=lr,
                  training_loader=training_loader, validation_query_data=validation_query_data,
                  validation_gallery_data=validation_gallery_data, total_steps=25000,
                  steps_between_validations=steps_between_validations, device=device, image_size=image_size,
                  validation_batch_size=512)


@resnet.command()
@click.argument('dataset_type', type=click.Choice(['mars', 'market']))
@click.argument('dataset_path', type=click.Path(exists=True, file_okay=False))
@click.argument('model_path', type=click.Path(dir_okay=False))
@click.argument('output_path', type=click.Path(dir_okay=False))
@click.option('--batch-size', type=int, default=8)
@click.option('--persons-limit', type=int)
@click.option('--multiple-predictions', is_flag=True)
@click.option('--cpu', is_flag=True)
def predict_triplet(dataset_type: str, dataset_path: str, model_path: str, output_path: str, batch_size: int,
                    persons_limit: Optional[int], multiple_predictions: bool, cpu: bool):
    import json
    import random

    import torch
    import numpy as np

    from reidentification.evaluation.mars import generate_triplet_predictions
    from reidentification.models.resnet50 import ResNet50
    from reidentification.generators.loaders.mars import MarsLoader
    from reidentification.generators.loaders.market1501 import MarketLoader

    np.random.seed(42)
    random.seed(42)

    image_size = (128, 256)
    device = torch.device('cuda') if not cpu else torch.device('cpu')

    if dataset_type == 'mars':
        loader = MarsLoader(dataset_path, 1)
    elif dataset_type == 'market':
        loader = MarketLoader(dataset_path, 1)
    else:
        raise NotImplementedError

    model = ResNet50()
    if not cpu:
        model = torch.nn.DataParallel(model).to(device)
    model.load_state_dict(torch.load(model_path))
    model.eval()

    query_data = loader.query_data
    gallery_data = loader.gallery_data

    predictions = generate_triplet_predictions(query_data, gallery_data, model, image_size, batch_size,
                                               force_image_size=True, persons_limit=persons_limit, device=device)
    with open(output_path, 'w') as output_file:
        json.dump(predictions, output_file)
