import click
from typing import Optional


@click.group()
def mobilenet():
    pass


@mobilenet.command()
@click.argument('dataset_type', type=click.Choice(['mars', 'market']))
@click.argument('dataset_path', type=click.Path(exists=True, file_okay=False))
@click.argument('model_path', type=click.Path(dir_okay=False))
@click.option('--lr', type=float, default=3e-05)
@click.option('--eps', type=float, default=1e-03)
@click.option('--persons-per-batch', type=int, default=32)
@click.option('--images-per-person', type=int, default=4)
@click.option('--continue-training', is_flag=True)
@click.option('--pretrained-weights', type=click.Path(exists=True, dir_okay=False), default=None)
@click.option('--cpu', is_flag=True)
def train_triplet(dataset_type: str, dataset_path: str, model_path: str, lr: float, eps: float, persons_per_batch: int,
                  images_per_person: int, continue_training: bool, pretrained_weights: str, cpu: bool):
    import random

    import numpy as np
    import torch.utils.data
    import torch.utils.data.sampler

    from reidentification.generators.triplet_batch_hard import TripletSequence, TripletSampler
    from reidentification.models.mobilenet_v2 import MobileNetV2
    from reidentification.training import pytorch
    from reidentification.generators.loaders.mars import MarsLoader
    from reidentification.generators.loaders.market1501 import MarketLoader

    from reidentification.losses.triplet_batch_hard import BatchHardTripletLoss

    image_size = (224, 224)

    np.random.seed(2018)
    random.seed(2018)

    device = torch.device('cuda') if not cpu else torch.device('cpu')

    if dataset_type == 'mars':
        loader = MarsLoader(dataset_path, images_per_person)
    elif dataset_type == 'market':
        loader = MarketLoader(dataset_path, images_per_person)
    else:
        raise NotImplementedError

    training_images, training_persons_ranges = loader.training_data
    training_sequence = TripletSequence(images=training_images, persons_ranges=training_persons_ranges,
                                        image_size=image_size, resize_images=True, device=device)
    validation_images, validation_persons_ranges = loader.validation_data
    validation_sequence = TripletSequence(images=validation_images, persons_ranges=validation_persons_ranges,
                                          image_size=image_size, resize_images=True, device=device)

    training_loader = torch.utils.data.DataLoader(
        training_sequence, batch_sampler=TripletSampler(training_sequence, persons_per_batch, images_per_person),
        num_workers=4, pin_memory=True
    )
    validation_loader = torch.utils.data.DataLoader(
        validation_sequence, batch_sampler=TripletSampler(validation_sequence, persons_per_batch, images_per_person),
        num_workers=4, pin_memory=True
    )

    batch_hard_triplet_loss = BatchHardTripletLoss(device)

    model = MobileNetV2(pretrained_weights=pretrained_weights)
    if not cpu:
        model = torch.nn.DataParallel(model).to(device)
    if continue_training:
        model.load_state_dict(torch.load(model_path))

    optimizer = torch.optim.Adam(model.parameters(), lr=lr, eps=eps, weight_decay=0.0005)
    pytorch.train(model=model, model_path=model_path, loss_function=batch_hard_triplet_loss,
                  optimizer=optimizer, initial_learning_rate=lr,
                  training_loader=training_loader, validation_loader=validation_loader, epochs=5000, device=device)


@mobilenet.command()
@click.argument('dataset_path', type=click.Path(exists=True, file_okay=False))
@click.argument('model_path', type=click.Path(dir_okay=False))
@click.argument('output_path', type=click.Path(dir_okay=False))
@click.option('--batch-size', type=int, default=8)
@click.option('--persons-limit', type=int)
@click.option('--multiple-predictions', is_flag=True)
@click.option('--cpu', is_flag=True)
def predict_triplet(dataset_path: str, model_path: str, output_path: str, batch_size: int,
                    persons_limit: Optional[int], multiple_predictions: bool, cpu: bool):
    import json
    import random

    import torch
    import numpy as np

    from reidentification.evaluation.mars import generate_triplet_predictions
    from reidentification.models.mobilenet_v2 import MobileNetV2

    np.random.seed(2018)
    random.seed(2018)

    image_size = (224, 224)
    device = torch.device('cuda') if not cpu else torch.device('cpu')

    model = MobileNetV2()
    if not cpu:
        model = torch.nn.DataParallel(model).to(device)
    model.load_state_dict(torch.load(model_path))
    model.eval()
    predictions = generate_triplet_predictions(dataset_path, model, image_size, batch_size,
                                               force_image_size=True, persons_limit=persons_limit,
                                               single_image=not multiple_predictions)
    with open(output_path, 'w') as output_file:
        json.dump(predictions, output_file)
