import click


@click.group()
def profiling():
    pass


@profiling.command()
@click.argument('model_path', type=click.Path(dir_okay=False))
@click.option('--batch-size', type=int, default=8)
@click.option('--square-input', is_flag=True)
def prediction(model_path: str, batch_size: int, square_input: bool):
    import random
    import time

    import numpy as np
    import torch

    from torch import nn

    np.random.seed(2018)
    random.seed(2018)

    if square_input:
        image_size = (224, 224)
    else:
        image_size = (256, 128)

    input_shape = (3, *image_size)

    time_sum = 0
    model: nn.Module = torch.load(model_path)
    batch = np.ndarray((batch_size, *input_shape), dtype=np.float32)
    with torch.no_grad():
        for i in range(32000 // batch_size):
            start_time = time.perf_counter()
            model(batch)
            time_sum += time.perf_counter() - start_time

            print(f'\r{i + 1}: {time_sum / (i + 1) / batch_size}', end='')

    print()
    print(f'Mean sample prediction time for batch size {batch_size}: {time_sum / (i + 1) / batch_size}')
