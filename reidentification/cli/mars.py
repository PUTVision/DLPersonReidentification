import click


@click.group()
def mars():
    pass


@mars.command()
@click.argument('dataset_path', type=click.Path(exists=True, file_okay=False))
def get_statistics(dataset_path: str):
    from reidentification.evaluation.mars import build_files_tree

    print('PersonID;Cameras;Images')
    tree = build_files_tree(dataset_path, single_image=False)
    for person_id, person_cameras in tree.items():
        person_images_count = 0
        for camera_id, camera_images in person_cameras.items():
            person_images_count += len(camera_images)

        print(f'{person_id};{len(person_cameras)};{person_images_count}')
