import time
import torch
from collections import defaultdict

import numpy as np
import os
import random
import re
from torch import nn
from typing import List, Dict, Tuple, Optional, Union, FrozenSet

from reidentification.generators.loaders import PersonsTree
from reidentification.predictors.predictor import Predictor
from reidentification.utils.image import load_image

Predictions = Dict[int, Dict[int, Dict[int, List[Tuple[int, float]]]]]


def predict_vectors(dataset_dir: str, model: nn.Module, image_size: Tuple[int, int],
                    force_image_size: bool=False, persons_limit: Optional[int]=None,
                    single_image: bool=True) -> Dict[str, List[np.ndarray]]:
    forced_image_size = image_size if force_image_size else None
    input_shape = (3, *image_size)

    files_tree = build_files_tree(dataset_dir, single_image)
    if persons_limit is not None:
        files_tree = random.sample(files_tree.items(), persons_limit)
    else:
        files_tree = files_tree.items()

    persons_count = len(files_tree)
    predictions: Dict[str, List[np.ndarray]] = defaultdict(lambda: {})
    for i, (person_name, person_cameras) in enumerate(files_tree):
        print(f'\r{i + 1}/{persons_count}', end='')
        for camera_id, camera_image_paths in person_cameras.items():
            if single_image:
                camera_image_paths = [camera_image_paths]

            input_batch = np.empty((len(camera_image_paths), *input_shape), dtype=np.float32)
            for j, camera_image_path in enumerate(camera_image_paths):
                input_batch[j] = load_image(camera_image_path, forced_image_size)

            predictions[person_name][camera_id] = list(model.predict(input_batch))
    print()

    return dict(predictions)


def _predict_vectors(files_tree: Dict[int, Dict[int, List[str]]], model: nn.Module,
                     image_size: Tuple[int, int], batch_size: int, device: torch.device, force_image_size: bool=False,
                     persons_limit: Optional[int]=None,
                     quiet: bool=False) -> Dict[int, Dict[int, List[np.ndarray]]]:
    input_shape = (3, *image_size)
    forced_image_size = image_size if force_image_size else None

    if persons_limit is not None:
        files_tree = random.sample(files_tree.items(), persons_limit)
    else:
        files_tree = files_tree.items()

    persons_count = len(files_tree)
    predictions: Dict[int, Dict[int, List[np.ndarray]]] = defaultdict(lambda: defaultdict(lambda: []))
    persons_ids_batch: List[int] = []
    images_batch = torch.empty(batch_size, *input_shape, dtype=torch.float32, device=device)
    cameras_ids_batch: List[int] = []

    position_in_batch = 0
    for i, (person_id, person_cameras) in enumerate(files_tree):
        if not quiet:
            print(f'\r{i + 1}/{persons_count}', end='')

        for camera_id, camera_images in person_cameras.items():
            for camera_image in camera_images:
                if position_in_batch == batch_size:
                    batch_predictions = model(images_batch)
                    for prediction_vector, prediction_person_id, prediction_camera_id in zip(batch_predictions,
                                                                                             persons_ids_batch,
                                                                                             cameras_ids_batch):
                        predictions[prediction_person_id][prediction_camera_id].append(prediction_vector.cpu().numpy())

                    persons_ids_batch.clear()
                    cameras_ids_batch.clear()
                    position_in_batch = 0

                images_batch[position_in_batch] = load_image(camera_image, forced_image_size)
                persons_ids_batch.append(person_id)
                cameras_ids_batch.append(camera_id)
                position_in_batch += 1

    if not quiet:
        print()

    left_predictions = model(images_batch)
    for index in range(position_in_batch):
        vector = left_predictions[index]
        person_id = persons_ids_batch[index]
        camera_id = cameras_ids_batch[index]
        predictions[person_id][camera_id].append(vector.cpu().numpy())

    return predictions


def _get_distance(first_vector: np.ndarray, second_vector: np.ndarray,
                  kissme_matrix: Optional[np.ndarray] = None) -> float:
    if kissme_matrix is not None:
        diff = (second_vector - first_vector).reshape((128, 1))
        return 1 / (1 + float(np.squeeze(diff.T @ kissme_matrix @ diff)))
    else:
        return 1 / (1 + float(np.sum(np.square(second_vector - first_vector))))


def generate_triplet_predictions(query_persons: PersonsTree = None, gallery_persons: PersonsTree = None,
                                 model: Optional[nn.Module] = None,
                                 image_size: Optional[Tuple[int, int]] = None, batch_size: Optional[int] = None,
                                 force_image_size: bool = False, persons_limit: Optional[int] = None,
                                 query_persons_vectors: Optional[Dict[int, Dict[int, np.ndarray]]] = None,
                                 gallery_persons_vectors: Optional[Dict[int, Dict[int, np.ndarray]]] = None,
                                 kissme_matrix: Optional[np.ndarray] = None,
                                 device: torch.device = None,
                                 quiet: bool = False) -> Predictions:
    predictions: Predictions = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: [])))

    if query_persons_vectors is None or gallery_persons_vectors is None:
        with torch.no_grad():
            query_persons_vectors = _predict_vectors(query_persons, model, image_size, batch_size, device,
                                                     force_image_size, persons_limit, quiet)
            gallery_persons_vectors = _predict_vectors(gallery_persons, model, image_size, batch_size, device,
                                                       force_image_size, persons_limit, quiet)

    query_persons_count = len(query_persons_vectors)
    for i, (query_person_id, query_person_cameras) in enumerate(query_persons_vectors.items()):
        if not quiet:
            print(f'\r{i + 1}/{query_persons_count}', end='')

        for query_camera_id, query_camera_vectors in query_person_cameras.items():
            assert len(query_camera_vectors) == 1
            query_vector = query_camera_vectors[0]

            cameras_to_process = gallery_persons_vectors[query_person_id].keys() - {query_camera_id}
            for gallery_person_id, gallery_person_cameras in gallery_persons_vectors.items():
                for gallery_camera_id, gallery_camera_vectors in gallery_person_cameras.items():
                    if gallery_camera_id in cameras_to_process:
                        for gallery_vector in gallery_camera_vectors:
                            distance = _get_distance(query_vector, gallery_vector, kissme_matrix)
                            predictions[query_person_id][query_camera_id][gallery_camera_id].append(
                                (gallery_person_id, distance)
                            )
    if not quiet:
        print()

    return predictions


def generate_predictions(dataset_dir: str, predictor: Predictor, image_size: Tuple[int, int],
                         batch_size: int, force_image_size: bool=False, segmented: bool=False) -> Predictions:
    input_shape = (3, *image_size)
    forced_image_size = image_size if force_image_size else None

    files_tree = build_files_tree(dataset_dir, segmented=segmented)
    predictions: Predictions = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: [])))
    first_input_batch = np.ndarray(shape=(batch_size, *input_shape), dtype=np.float32)
    second_input_batch = np.ndarray(shape=(batch_size, *input_shape), dtype=np.float32)
    source_names_batch = []
    target_names_batch = []
    source_camera_ids_batch = []
    target_camera_ids_batch = []
    pairs_indices_batch = []
    position_in_batch = 0
    time_start = time.time()
    compared_pairs: Dict[FrozenSet[Union[str, int]], float] = {}
    for i, (person_name, person_cameras) in enumerate(files_tree.items()):
        if i == 0:
            time_left_str = 'unknown'
        else:
            time_left = (time.time() - time_start) / (i + 0.000001) * (len(files_tree) - i)
            time_left_str = time.strftime("%H:%M:%S", time.gmtime(time_left))
        print(f'\rPerson {i + 1}/{len(files_tree)}; time left: {time_left_str}', end='')

        for source_camera_id, camera_image in person_cameras.items():
            random_image = load_image(camera_image, forced_image_size)
            for target_camera_id, other_camera_image in _get_other_cameras(person_cameras, source_camera_id).items():
                if position_in_batch >= batch_size:
                    person_predictions = predictor.predict(first_input_batch, second_input_batch)
                    for index, prediction in enumerate(person_predictions):
                        source_name = source_names_batch[index]
                        target_name = target_names_batch[index]
                        source_camera_id_ = source_camera_ids_batch[index]
                        target_camera_id_ = target_camera_ids_batch[index]
                        pair_index_ = pairs_indices_batch[index]

                        compared_pairs[pair_index_] = float(prediction)
                        predictions[source_name][source_camera_id_][target_camera_id_].append(
                            (target_name, float(prediction))
                        )

                    position_in_batch = 0
                    source_names_batch.clear()
                    target_names_batch.clear()
                    source_camera_ids_batch.clear()
                    target_camera_ids_batch.clear()
                    pairs_indices_batch.clear()

                pair_index = frozenset((person_name, source_camera_id, person_name, target_camera_id))
                if pair_index in compared_pairs:
                    predictions[person_name][source_camera_id][target_camera_id].append(
                        (person_name, compared_pairs[pair_index])
                    )
                    continue

                first_input_batch[position_in_batch] = random_image
                second_input_batch[position_in_batch] = load_image(other_camera_image, forced_image_size)
                source_names_batch.append(person_name)
                target_names_batch.append(person_name)
                source_camera_ids_batch.append(source_camera_id)
                target_camera_ids_batch.append(target_camera_id)
                pairs_indices_batch.append(pair_index)

                position_in_batch += 1

            for other_person_name, other_person_cameras in _get_other_persons(files_tree, person_name).items():
                for target_camera_id, other_person_camera_image in other_person_cameras.items():
                    if target_camera_id not in _get_other_cameras(person_cameras, source_camera_id):
                        # We don't need to predict cameras that didn't capture the source person
                        # Source camera is ignored as we only evaluate cross-camera
                        continue

                    if position_in_batch >= batch_size:
                        person_predictions = predictor.predict(first_input_batch, second_input_batch)
                        for index, prediction in enumerate(person_predictions):
                            source_name = source_names_batch[index]
                            target_name = target_names_batch[index]
                            source_camera_id = source_camera_ids_batch[index]
                            target_camera_id = target_camera_ids_batch[index]
                            pair_index = pairs_indices_batch[index]

                            compared_pairs[pair_index] = float(prediction)
                            predictions[source_name][source_camera_id][target_camera_id].append(
                                (target_name, float(prediction))
                            )

                        position_in_batch = 0
                        source_names_batch.clear()
                        target_names_batch.clear()
                        source_camera_ids_batch.clear()
                        target_camera_ids_batch.clear()
                        pairs_indices_batch.clear()

                    pair_index = frozenset((person_name, source_camera_id, other_person_name, target_camera_id))
                    if pair_index in compared_pairs:
                        predictions[person_name][source_camera_id][target_camera_id].append(
                            (other_person_name, compared_pairs[pair_index])
                        )
                        continue

                    first_input_batch[position_in_batch] = random_image
                    second_input_batch[position_in_batch] = load_image(other_person_camera_image, forced_image_size)
                    source_names_batch.append(person_name)
                    target_names_batch.append(other_person_name)
                    source_camera_ids_batch.append(source_camera_id)
                    target_camera_ids_batch.append(target_camera_id)
                    pairs_indices_batch.append(pair_index)

                    position_in_batch += 1

    person_predictions = predictor.predict(first_input_batch, second_input_batch)
    for index in range(len(source_names_batch)):
        source_name = source_names_batch[index]
        target_name = target_names_batch[index]
        source_camera_id_ = source_camera_ids_batch[index]
        target_camera_id_ = target_camera_ids_batch[index]

        predictions[source_name][source_camera_id_][target_camera_id_].append(
            (target_name, float(person_predictions[index]))
        )

    print()
    return predictions


def _get_other_cameras(person_cameras: Dict[int, Union[str, np.ndarray]], other_than: int):
    person_cameras = person_cameras.copy()
    del person_cameras[other_than]

    return person_cameras


def _get_other_persons(persons_tree: Dict[int, Dict[int, Union[str, np.ndarray]]], other_than: int):
    persons_tree = persons_tree.copy()
    del persons_tree[other_than]

    return persons_tree


def build_files_tree(dataset_dir: str, single_image: bool=True,
                     segmented: bool=False) -> Dict[str, Dict[int, Union[str, List[str]]]]:
    files_tree = defaultdict(lambda: {})
    for person_dir in os.scandir(dataset_dir):
        cameras_images = defaultdict(lambda: [])
        for person_image in os.scandir(person_dir):
            if person_image.is_file() and ('masked' in person_image.name) is segmented:
                camera_id = int(re.search(r'C(\d)', person_image.name).group(1))
                cameras_images[camera_id].append(person_image.path)

        for person_camera_id, person_camera_images in cameras_images.items():
            if single_image:
                files_tree[person_dir.name][person_camera_id] = random.choice(person_camera_images)
            else:
                files_tree[person_dir.name][person_camera_id] = person_camera_images

    return files_tree
