import torch.nn.functional as F

from torch import nn
from reidentification.models.applications.resnet import resnet50


class ResNet50(nn.Module):
    def __init__(self, pretrained: bool=False):
        super().__init__()

        self.resnet = resnet50(pretrained=pretrained)
        self.fc1 = nn.Linear(2048, 1024)
        self.batch_norm = nn.BatchNorm1d(1024)
        self.relu = nn.ReLU(inplace=True)
        self.fc2 = nn.Linear(1024, 128)

        nn.init.constant_(self.batch_norm.weight, 1)
        nn.init.constant_(self.batch_norm.bias, 0)

    def forward(self, x):
        x = self.resnet(x)
        x = F.avg_pool2d(x, x.size()[2:])
        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        x = self.batch_norm(x)
        x = self.relu(x)
        x = self.fc2(x)
        return x
