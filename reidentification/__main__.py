import click

from reidentification.cli.mars import mars
from reidentification.cli.mobilenet import mobilenet
from reidentification.cli.profiling import profiling
from reidentification.cli.resnet import resnet
from reidentification.cli.utils import utils


@click.group()
def cli():
    pass


cli.add_command(resnet)
cli.add_command(mobilenet)
cli.add_command(utils)
cli.add_command(profiling)
cli.add_command(mars)


if __name__ == '__main__':
    cli()
