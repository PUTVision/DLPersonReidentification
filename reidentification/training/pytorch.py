import os
from typing import Callable, Any, Optional, Tuple

import torch.utils.data

from reidentification.evaluation.mars import generate_triplet_predictions
from reidentification.generators.loaders import PersonsTree
from reidentification.losses.triplet_batch_hard_2 import global_loss
from reidentification.metrics.rank_accuracy import get_rank_accuracy


def train(model: torch.nn.Module, model_path: str, loss_function: Callable,
          optimizer: torch.optim.Optimizer, initial_learning_rate: float,
          training_loader: torch.utils.data.DataLoader, validation_query_data: PersonsTree,
          validation_gallery_data: PersonsTree, total_steps: int, steps_between_validations: int, device: torch.device,
          image_size: Tuple[int, int], validation_batch_size: int, scheduler: Optional[Any]=None):
    best_validation_accuracy = validate(model, validation_query_data, validation_gallery_data, image_size,
                                        validation_batch_size, device)
    print(f'Starting with validation accuracy {best_validation_accuracy}')

    current_step = 0
    while current_step < total_steps:
        model = model.train()

        if scheduler is not None:
            scheduler.step()

        loss_sum = 0.0

        for i, (inputs, labels) in enumerate(training_loader):
            labels = labels.to(device)

            _adjust_learning_rate(optimizer, current_step, 15000, total_steps, initial_learning_rate)

            optimizer.zero_grad()

            with torch.enable_grad():
                outputs = model(inputs)
                loss, p_inds, n_inds, dist_ap, dist_an, dist_mat = global_loss(loss_function, outputs, labels, False)
                loss.backward()
                optimizer.step()

            loss_sum += loss.item()
            current_step += 1

            print(f'\rStep {current_step + 1}: Loss: {loss_sum / (i + 1):.4f}', end='')

            if current_step and current_step % steps_between_validations == 0:
                temporary_path, extension = os.path.splitext(model_path)
                checkpoint_path = f'{temporary_path}_{current_step}{extension}'
                torch.save(model.state_dict(), checkpoint_path)

                validation_accuracy = validate(model, validation_query_data, validation_gallery_data, image_size,
                                               validation_batch_size, device)
                print(f'\nStep {current_step + 1}: Validation accuracy: {validation_accuracy:.4f}')
                if validation_accuracy > best_validation_accuracy:
                    print('Validation accuracy improved - saving model', end='\n\n')
                    torch.save(model.state_dict(), model_path)
                    best_validation_accuracy = validation_accuracy
                else:
                    print()


def validate(model: torch.nn.Module, validation_query_data: PersonsTree, validation_gallery_data: PersonsTree,
             image_size: Tuple[int, int], batch_size: int, device: torch.device) -> float:
    model = model.eval()
    with torch.no_grad():
        predictions = generate_triplet_predictions(validation_query_data, validation_gallery_data, model, image_size,
                                                   batch_size, force_image_size=True, device=device, quiet=True)

    return get_rank_accuracy(predictions, 1)


def _adjust_learning_rate(optimizer, current_step: int, start_step: int, end_step: int, initial_lr: float):
    if current_step <= start_step:
        return initial_lr

    lr = initial_lr * pow(0.001, (current_step - start_step) / (end_step - start_step))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
