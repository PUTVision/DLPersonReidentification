import pickle
import numpy as np
from sklearn import preprocessing

with open('positives.p', 'rb') as output_file:
    positives = pickle.load(output_file)

with open('negatives.p', 'rb') as output_file:
    negatives = pickle.load(output_file)

positives = np.array(positives)
negatives = np.array(negatives)

pos_diff = positives[:, 0, :] - positives[:, 1, :]
neg_diff = negatives[:, 0, :] - negatives[:, 1, :]

# pos_diff = preprocessing.normalize(positives[:, 0, :]) - preprocessing.normalize(positives[:, 1, :])
# neg_diff = preprocessing.normalize(negatives[:, 0, :]) - preprocessing.normalize(negatives[:, 1, :])

pos_diff = pos_diff[:, :, np.newaxis]
C_pos = np.mean(pos_diff @ pos_diff.transpose((0, 2, 1)), axis=0)

neg_diff = neg_diff[:, :, np.newaxis]
C_neg = np.mean(neg_diff @ neg_diff.transpose((0, 2, 1)), axis=0)

M = np.linalg.inv(C_pos) - np.linalg.inv(C_neg)
print(np.amax(M))
print(np.amin(M))

pickle.dump(M, open("M.p", "wb"))
