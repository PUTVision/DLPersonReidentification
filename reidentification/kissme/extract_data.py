import pickle
import random
import itertools

vectors_path = 'predicted_vectors.p'

with open(vectors_path, 'rb') as output_file:
    predicted_vectors = pickle.load(output_file)

positives = []
for person, cameras in predicted_vectors.items():
    descriptor_list = list(cameras.values())
    pairs = list(itertools.combinations(descriptor_list, 2))
    positives.extend(pairs)

print(len(positives))
pickle.dump(positives, open("positives.p", "wb"))

positives_len = len(positives)
negatives_pairs = set()
negatives = []
while len(negatives) < positives_len:
    (person_1, person_1_cameras), (person_2, person_2_cameras) = random.sample(tuple(predicted_vectors.items()), 2)
    person_1_cam, vec_1 = random.choice(tuple(person_1_cameras.items()))
    person_2_cam, vec_2 = random.choice(tuple(person_2_cameras.items()))

    pair = (f'{person_1}_{person_1_cam}', f'{person_2}_{person_2_cam}')
    if pair not in negatives_pairs and reversed(pair) not in negatives_pairs:
        negatives_pairs.add(pair)
        negatives.append((vec_1, vec_2))

pickle.dump(list(negatives), open("negatives.p", "wb"))
print(len(negatives))


