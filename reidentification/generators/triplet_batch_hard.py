import numpy as np
import random
import torch.utils.data
import torch.utils.data.sampler
import torchvision
from PIL import Image
from typing import Tuple, List, Optional, Any


def _load_image(path: str, forced_size: Optional[Tuple[int, int]]=None) -> np.ndarray:
    transformations = []
    image = Image.open(path)
    if forced_size is not None:
        transformations.append(torchvision.transforms.Resize(forced_size))

    transformations.append(torchvision.transforms.RandomHorizontalFlip())
    transformations.append(torchvision.transforms.ToTensor())

    return torchvision.transforms.Compose(transformations)(image)


class PersonsCamerasImagesSequence(torch.utils.data.Dataset):
    def __init__(self, images: List[Tuple[str, str]], persons_ranges: List[range], image_size: Tuple[int, int],
                 device: Any, resize_images: bool=False):
        self.persons_ranges = persons_ranges

        self._images = images
        self._input_shape = (3, *image_size)
        self._forced_size = image_size if resize_images else None
        self._device = device

    def __len__(self) -> int:
        return len(self._images)

    def __getitem__(self, index: int) -> Tuple[torch.tensor, torch.tensor]:
        image_path, person_id = self._images[index]
        return _load_image(image_path, self._forced_size), person_id


class TripletSequence(torch.utils.data.Dataset):
    def __init__(self, images: List[Tuple[str, int]], persons_ranges: List[range], image_size: Tuple[int, int],
                 device: Any, resize_images: bool=False):
        self.persons_ranges = persons_ranges

        self._images = images
        self._input_shape = (3, *image_size)
        self._forced_size = image_size if resize_images else None
        self._device = device

    def __len__(self) -> int:
        return len(self._images)

    def __getitem__(self, index: int) -> Tuple[torch.tensor, torch.tensor]:
        image_path, person_id = self._images[index]
        return _load_image(image_path, self._forced_size), person_id


class TripletSampler(torch.utils.data.sampler.SequentialSampler):
    def __init__(self, data_source: TripletSequence, persons_per_batch: int, images_per_person: int):
        super().__init__(data_source)

        self._data_source = data_source
        self._persons_per_batch = persons_per_batch
        self._images_per_person = images_per_person
        self._batch_size: int = self._persons_per_batch * self._images_per_person

    def __iter__(self):
        persons_indices = list(range(len(self._data_source.persons_ranges)))
        random.shuffle(persons_indices)

        batch = []
        for person_index in persons_indices:
            if len(batch) == self._batch_size:
                yield batch
                batch = []

            try:
                batch.extend(random.sample(self._data_source.persons_ranges[person_index], self._images_per_person))
            except ValueError:
                # Not enough images
                pass

    def __len__(self):
        return len(self._data_source.persons_ranges) // self._persons_per_batch
