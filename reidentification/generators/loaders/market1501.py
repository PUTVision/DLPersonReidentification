from collections import defaultdict

import os
import re
from typing import Dict, List, Tuple, Set

from reidentification.generators.loaders import PersonsTree
from reidentification.generators.loaders.loader import Loader

IMAGE_SIZE = (128, 256)


class MarketLoader(Loader):
    def __init__(self, path: str, min_images_per_person: int):
        training_people = self._get_training_images(path)
        query_people, gallery_people = self._get_testing_images(path)
        super().__init__(training_people, query_people, gallery_people, min_images_per_person)

    @staticmethod
    def _get_training_images(path: str) -> Dict[int, Dict[int, List[str]]]:
        path = os.path.join(path, 'bounding_box_train')
        return MarketLoader._build_persons_tree(path)

    @staticmethod
    def _get_testing_images(path: str) -> Tuple[PersonsTree, PersonsTree]:
        query_path = os.path.join(path, 'query')
        manual_bbox_path = os.path.join(path, 'gt_bbox')
        gallery_path = os.path.join(path, 'bounding_box_test')

        query_filenames = {file.name for file in os.scandir(query_path)}
        query_people = MarketLoader._build_persons_tree(manual_bbox_path, query_filenames)
        gallery_people = MarketLoader._build_persons_tree(gallery_path)

        return query_people, gallery_people

    @staticmethod
    def _build_persons_tree(path: str, allowed_filenames: Set[str] = None) -> Dict[int, Dict[int, List[str]]]:
        persons_images = defaultdict(lambda: defaultdict(lambda: []))
        for entry in os.scandir(path):
            if entry.name.startswith('-1') or (allowed_filenames is not None and entry.name not in allowed_filenames):
                continue

            match = re.match('(\d+)_c(\d)s\d_\d+_\d+.jpg', entry.name)
            if match is not None:
                persons_images[int(match.group(1))][int(match.group(2))].append(entry.path)

        return persons_images
