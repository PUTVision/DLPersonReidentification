import random
from itertools import repeat
from typing import List, Tuple, Dict

from sklearn.model_selection import train_test_split

from reidentification.generators.loaders import PersonsTree


class Loader:
    def __init__(self, training_persons: PersonsTree, query_persons: PersonsTree, gallery_persons: PersonsTree,
                 min_images_per_person: int):
        self._query_persons = query_persons
        self._gallery_persons = gallery_persons
        self._min_images_per_person = min_images_per_person

        training_persons_ids, validation_persons_ids = train_test_split(
            tuple(training_persons.keys()), test_size=0.25, random_state=2018
        )
        self._training_persons = {k: training_persons[k] for k in training_persons_ids}

        self._validation_gallery_persons = {k: training_persons[k] for k in validation_persons_ids}
        self._validation_query_persons = self._get_validation_query_persons(self._validation_gallery_persons)

    @staticmethod
    def _get_validation_query_persons(validation_persons: PersonsTree) -> PersonsTree:
        validation_query_persons = {}
        for person_id, person_cameras in validation_persons.items():
            validation_query_persons[person_id] = {}
            for camera_id, camera_images in person_cameras.items():
                random_image = random.choice(camera_images)
                camera_images.remove(random_image)

                validation_query_persons[person_id][camera_id] = [random_image]

        return validation_query_persons

    @property
    def data(self) -> PersonsTree:
        return self._training_persons

    @property
    def training_data(self) -> Tuple[List[Tuple[str, int]], List[range]]:
        images = []
        persons_ranges = []
        for person_id, person_cameras in self._training_persons.items():
            person_images = [camera_image for camera_images in person_cameras.values()
                             for camera_image in camera_images]
            if len(person_images) >= self._min_images_per_person:
                range_start = len(images)
                images.extend(zip(person_images, repeat(person_id)))
                persons_ranges.append(range(range_start, len(images)))

        return images, persons_ranges

    @property
    def validation_query_data(self) -> PersonsTree:
        return self._validation_query_persons

    @property
    def validation_gallery_data(self) -> PersonsTree:
        return self._validation_gallery_persons

    @property
    def query_data(self) -> PersonsTree:
        return self._query_persons

    @property
    def gallery_data(self) -> PersonsTree:
        return self._gallery_persons
