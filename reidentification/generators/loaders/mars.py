from collections import defaultdict

import os
import re
from typing import Dict, List

from reidentification.generators.loaders.loader import Loader


class MarsLoader(Loader):
    def __init__(self, path: str, min_images_per_person: int):
        super().__init__(self._get_images(path), min_images_per_person)

    @staticmethod
    def _get_images(path: str) -> Dict[int, Dict[int, List[str]]]:
        persons_images = defaultdict(lambda: defaultdict(lambda: []))
        for person_dir in filter(lambda d: d.is_dir(), os.scandir(path)):
            for entry in os.scandir(person_dir.path):
                match = re.match('.+C(\d).+.jpg', entry.name)
                if match is not None:
                    persons_images[int(person_dir.name)][int(match.group(1))].append(entry.path)

        return persons_images
